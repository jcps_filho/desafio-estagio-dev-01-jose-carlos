<?php

    require_once('db.class.php');

    //Recebe as informações do formulario de cadastro
    $nome =  $_POST['nome'];

    $usuario = $_POST['usuario'];
    
    $senha = $_POST['senha'];

    //Obtem o tamanho da string da senha
    $strLen = mb_strlen($senha);

    //Procura no banco um nome de usuário igual ao cadastrado
    $sql1 = "SELECT * FROM usuarios WHERE usuario = '$usuario';";

    //Faz o link com o banco
    $objDB = new db();
    $link = $objDB->conecta_mysql();

    $resultado_id = mysqli_query($link, $sql1);

    //Faz a validação pra ver se o usuário já existe no banco
    if($resultado_id){
        $dados_usuario = mysqli_fetch_array($resultado_id);
        if(isset($dados_usuario['usuario']) and $dados_usuario['usuario'] == $usuario){
            header('Location: cadastro.php?erro=3');
        }
    }
    //Faz a validação da senha
    if(preg_match('/\p{Lu}/u', $senha) || preg_match('/[0-9]/', $senha)){
        if($strLen == 8)
            $sql = "INSERT INTO usuarios (nome, usuario, senha) values ('$nome', '$usuario', '$senha');";
        else
        header('Location: cadastro.php?erro=2'); 
    } else {
        header('Location: cadastro.php?erro=2');
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Cadastro</title>
</head>
<body>
    <?php
        //executar a query
        if(mysqli_query($link, $sql)){
            echo '<span style="font-size: 20px;">Usuário registrado com sucesso!</span>';
        } else {
            echo '<span style="font-size: 20px;">Erro ao registrar o usuário!</span>';
        }
    ?>
    <br>
    <a href="index.php">Acessar Painel</a>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>