<?php

    $erro = isset($_GET['erro']) ? $_GET['erro'] : 0;

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Cadastro</title>
</head>
<body>
<div class="container">
    <h1>Cadastro</h1>
        <form method="post" action="registra_usuario.php" class="col-md-4">
            <div class="form-group" action="">
                <label for="nome"><strong>Nome completo*:</strong></label>
                <input type="text" class="form-control" id="nome" name="nome" required>
            </div>
            <div class="form-group">
                <label for="usuario"><strong>Usuário*:</strong></label>
                <input type="text" class="form-control" id="usuario" name="usuario" required>
            </div>
            <div class="form-group"
                <label for="senha"><strong>Senha*:</strong></label>
                <input type="password" class="form-control" id="senha" name="senha" placeholder='8 caracteres' required>
                </div>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </form>
                <?php
                    if($erro == 2) 
                        echo 'A sua senha deve ter 8 caracteres e conter pelo menos: uma letra maiúscula e um número!';
                    if($erro == 3)
                        echo 'O usuário ja existe, por favor escolha outro!';
                ?>
            </div>
        </div>
    </div>
</div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>